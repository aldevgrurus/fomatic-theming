import './App.css';
import './assets/fomantic/dist/semantic.css';

import { Container, Header, Loader } from 'semantic-ui-react';
import ThemingLayout from "./ThemingLayout";
function App() {
  return (
    <Container>
      <Header>
        Fomantic-UI & Semantic-UI-React
      </Header>
      <Loader active inline className="slow red" />
      <Loader active inline className="fast green" />
      <ThemingLayout></ThemingLayout>
    </Container>
  );
}

export default App;
